package edu.a256devs.p0121_logandmess;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView tvOut;
    Button btnOk;
    Button btnCancel;

    private static final String TAG = "myLogs"; //FIXME

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "Find View IDs.");   //FIXME
        tvOut = (TextView) findViewById(R.id.tvOut);
        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        Log.d(TAG, "Setup listener.");  //FIXME
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "Switch by button ID");  //FIXME
        switch (v.getId()) {
            case R.id.btnOk:
                Log.d(TAG, "You pressed ОК");   //FIXME
                Toast.makeText(this, "You pressed ОК", Toast.LENGTH_LONG).show();
                tvOut.setText("You pressed ОК");
                break;

            case R.id.btnCancel:
                Log.d(TAG, "You pressed Cancel");   //FIXME
                Toast.makeText(this, "You pressed Cancel", Toast.LENGTH_LONG).show();
                tvOut.setText("You pressed Cancel");
                break;
        }
    }
}
