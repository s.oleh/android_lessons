package edu.a256devs.twoactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etFName;
    RadioGroup rgGravity;
    Button btnSubmit;
    SeekBar seekValue;
    CheckBox bCheck;
    Switch bSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etFName = (EditText) findViewById(R.id.etFName);
        rgGravity = (RadioGroup) findViewById(R.id.rgGravity);
        seekValue = (SeekBar) findViewById(R.id.seekBar);
        bCheck = (CheckBox) findViewById(R.id.checkBox);
        bSwitch = (Switch) findViewById(R.id.switch1);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ViewActivity.class);

        String sRadio = "undefined";
        switch (rgGravity.getCheckedRadioButtonId()) {
            case R.id.rbLeft:
                sRadio = "Left";
                break;

            case R.id.rbRight:
                sRadio = "Right";
                break;
        }

        intent.putExtra("fname", etFName.getText().toString());
        intent.putExtra("radio", sRadio);
        intent.putExtra("checkbox", (bCheck.isChecked() ? "Checked" : "Not set"));
        intent.putExtra("switch", (bSwitch.isChecked() ? "On" : "Off"));
        intent.putExtra("seek", Integer.toString(seekValue.getProgress()));


        startActivity(intent);
    }
}
