package edu.a256devs.twoactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ViewActivity extends AppCompatActivity {

    TextView tvView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        tvView = (TextView) findViewById(R.id.tvView);

        Intent intent = getIntent();
        String sOutput = "Your name is: " + intent.getStringExtra("fname");
        sOutput += "\nYour side is: " + intent.getStringExtra("radio");
        sOutput += "\nCheckbox status is: " + intent.getStringExtra("checkbox");
        sOutput += "\nSwitch is: " + intent.getStringExtra("switch");
        sOutput += "\nYour seek value = " + intent.getStringExtra("seek");

        tvView.setText(sOutput);
    }
}
