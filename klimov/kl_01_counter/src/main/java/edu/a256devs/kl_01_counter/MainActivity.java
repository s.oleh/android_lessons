package edu.a256devs.kl_01_counter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private Button mCrowsCounterButton;
    private TextView helloTextView;

    private int mCount = 0;
    private int mCountCat = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCrowsCounterButton = findViewById(R.id.button_counter);
        helloTextView = (TextView)findViewById(R.id.textView);

        mCrowsCounterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helloTextView.setText("I count " + ++mCount + " birds and " + mCountCat + " cats");
            }
        });
    }

    public void onClick(View view) {
        helloTextView.setText("Hello Kitty!");
    }

    public void onClickCat(View view) {
        helloTextView.setText("I count " + mCount + " birds and " + ++mCountCat + " cats");
    }
}
