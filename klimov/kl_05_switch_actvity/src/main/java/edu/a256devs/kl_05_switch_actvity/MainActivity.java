package edu.a256devs.kl_05_switch_actvity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, AboutActivity.class);
        startActivity(intent);
    }

    public void onClickSecond(View view) {
        EditText et_send_to = (EditText) findViewById(R.id.et_sent_to);
        EditText et_description = (EditText) findViewById(R.id.et_description);

        Intent intent = new Intent(MainActivity.this, SecondActivity.class);

        intent.putExtra("send_to", et_send_to.getText().toString());
        intent.putExtra("description", et_description.getText().toString());
        startActivity(intent);
    }
}
