package edu.a256devs.kl_05_switch_actvity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String sSendTo = getIntent().getExtras().getString("send_to");
        String sDescription = getIntent().getStringExtra("description");

        TextView infoTextView = (TextView)findViewById(R.id.tv_text);
        infoTextView.setText(sSendTo + " , вам передали " + sDescription);
    }
}
