package edu.a256devs.kl_04_traffic_light;

import android.app.Activity;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends Activity {

    private ConstraintLayout mConstraintLayout;
    private TextView mInfoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mConstraintLayout = (ConstraintLayout) findViewById(R.id.constraintLayout);
        mInfoTextView = (TextView) findViewById(R.id.textViewInfo);
    }

    public void onClickRed(View view) {
        mInfoTextView.setText(R.string.s_btn_red);
        mConstraintLayout.setBackgroundResource(R.color.redColor);
    }

    public void onClickYellow(View view) {
        mInfoTextView.setText(R.string.s_btn_yellow);
        mConstraintLayout.setBackgroundResource(R.color.yellowColor);
    }

    public void onClickGreen(View view) {
        mInfoTextView.setText(R.string.s_btn_green);
        mConstraintLayout.setBackgroundResource(R.color.greenColor);
    }
}
